import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { HeroList } from "../models/hero.list";
import { HeroDetail } from "../models/hero.detail";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private baseUrl = 'https://www.superheroapi.com/api.php/';
    
  constructor(private http: HttpClient) { }


  getheroList(id: number) : Observable<HeroList[]> {
    try{
      return this.http.get<HeroList[]>(this.baseUrl + '809039036705284/' + id)
    }catch(e){
      return e;
    }
  }

  getheroDetail(hero: string): Observable<HeroDetail> {
      return this.http.get<HeroDetail>(this.baseUrl + '809039036705284/' + '/search/' + hero);
  }

  checkImage(url:string): Observable<HeroDetail> {
    return this.http.get<HeroDetail>(url);
  }

  saveHero(hero:object): Observable<HeroDetail> {
    return this.http.post<HeroDetail>('http://localhost:3000', hero);
  }

  getAllHeroes(): Observable<HeroList[]> {
    return this.http.get<HeroList[]>('http://localhost:3000');
  }
}
