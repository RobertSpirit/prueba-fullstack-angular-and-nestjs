export class HeroList {

    response: string;
    id: string;
    name: string;
    powerstats: object;
    biography: {
        alignment : string;
    };
    appearance: object;
    work: object;
    connections: object;
    image: {
        url: string
    };

    constructor() {
        this.response = '';
        this.id = '';
        this.name = '';
        this.powerstats = {};
        this.biography = {
            alignment: ''
        };
        this.appearance = {};
        this.work = {};
        this.connections = {};
        this.image = {
            url : ''
        };
    }
}