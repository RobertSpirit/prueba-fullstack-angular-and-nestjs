import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';

import {HeroList} from '../../models/hero.list';
import {HeroService} from '../../services/hero-service.service';
import {HeroDetailComponent} from '../hero-detail/hero-detail.component'
import {forkJoin} from 'rxjs';



@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})
export class HeroListComponent implements OnInit {
  search: FormControl = new FormControl('');
  heros: HeroList[] = [];
  heroDB: HeroList[] = [];
  dbInfo: boolean = false;

  private scroll: number;
  private offset: number;
  private endset: number;

  isLoading: boolean;
  isLastPage = false;
  notification:boolean = false;
  message:string = 'Hero saved correctly';

  searchhero: HeroList[] = [];
  isSearching = false;

  constructor(private heroService: HeroService,
              private bottomSheet: MatBottomSheet,
              private snackBar: MatSnackBar) { 
                this.offset = 1 ;
                this.endset = 21 ;
                this.isLoading = false;
                this.scroll = 1;
              }

  ngOnInit(): void {
    
    this.heroService.getAllHeroes()
    .subscribe((herosDB: HeroList[]) => {
      this.heroDB = herosDB;
    });

    let observables = [];
    let state: any = [];
    for (let i = this.offset; i < this.endset; i++){            
      observables.push(this.heroService.getheroList(i))
    }  
    
    forkJoin(observables).subscribe(response => {
      response.forEach(_response => state.push(_response))
  }); 

  this.heros = state;
 
    
  }

  getPage(offset: number, endset:number) {    
      this.isLoading = true;

      let observables: any[] = [];
      for (let i = offset; i < endset; i++){      
        observables.push(this.heroService.getheroList(i))
      }  
      forkJoin(observables)
      .subscribe((dataArray) => {
         dataArray.map((element: any) => {
           if(!element.error){
            this.heros.push(element)
           }else{
             this.isLoading = false
           }
          });
      });  
  }

  getDBHeres() {
        this.dbInfo = !this.dbInfo;
        this.heroService.getAllHeroes()
        .subscribe((herosDB) => {
          this.heroDB = herosDB;
        });
  }
  
  onSearchhero(): void {
    const value = this.search.value;
    
    if(value === '') {
      this.isSearching = false;
    } else {
      this.isSearching = true;
      this.isLoading = true;
      this.heroService.getheroDetail(value)
      .subscribe((hero: any) => {
        this.searchhero = hero.results;
        this.isLoading = false;
      }, (error: any) => {
        this.isLoading = false;
        if(error.status === 404) {
          this.snackBar.open('Sorry, hero not found', 'Ok', {
            duration: 5000,
          });
        }
      })
    }
  }

  onScroll(event: Event): void {
    
    const element: HTMLDivElement = event.target as HTMLDivElement;

    let startScrol = (element.scrollTop  + element.offsetHeight);    
    if (startScrol < 1400*this.scroll){
      this.isLoading = false;
      this.scroll = this.scroll + 1;
    }
    
    if(element.scrollHeight - element.scrollTop < 1000) {
      this.offset = this.offset + 21;
      this.endset = this.endset + 21;
      this.getPage(this.offset, this.endset);
      this.scroll = this.scroll + 1;
    }
    
  }

  getPrincipalType(type: string): string {
    return type
  }

  onDetail(hero: any): void {
    this.bottomSheet.open(HeroDetailComponent, {
      data: {hero, dbInfo: this.dbInfo, color: this.ColorButtonFavorite(hero)}
    })
  }

  AddHero(hero: HeroList): void {
    const heroexist = this.heroDB.filter(x => x.id == hero.id)[0]
    if(heroexist){
      this.notification = true; 
      this.message = 'The hero was already saved';
      setTimeout(() => { this.notification = false;}, 2000);
    }else{      
      this.heroService.saveHero(hero)
      .subscribe((hero) => {
        if(hero){
          this.message = 'Hero saved correctly';
          this.notification = true;
          setTimeout(() => { 
            this.notification = false;
            window.location.reload();
          }, 1000);
        }        
      });

    }
   
  }

  ColorButtonFavorite(hero:HeroList): string{
    return this.heroDB.filter(value => value.id == hero.id)[0]? 'warning' : 'danger';
  } 

}
