import { Module } from '@nestjs/common';
import { HeroService } from './hero.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HeroSchema } from '../schemas/heroe.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Heroe', schema: HeroSchema }])],
  controllers: [],
  providers: [HeroService],
  exports: [HeroService, MongooseModule],
})

export class HeroModule {}
