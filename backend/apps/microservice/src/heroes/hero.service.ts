import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HeroDocument } from '../schemas/heroe.schema';

@Injectable()
export class HeroService {
  constructor(
    @InjectModel('Heroe') private readonly heroModel: Model<HeroDocument>,
  ) {}

  async getHeros(): Promise<object[]> {
    return this.heroModel.find({});
  }


  async saveHero(data): Promise<object> {
    const query = await this.heroModel.findOne({ id:data.id })
    if (!query) {
      const createdHero = new this.heroModel(data);
      return createdHero.save();
    }
    return { "message" : "Ya existe el heroe", "code" : " 500 " }
  }
}

