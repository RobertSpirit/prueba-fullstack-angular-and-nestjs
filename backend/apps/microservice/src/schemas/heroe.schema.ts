import { Type } from '@nestjs/common';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type HeroDocument = Hero & Document;

@Schema()
export class Hero {
  
  @Prop()
  id: String;

  @Prop()
  name: String;

  @Prop()
  powerstats: mongoose.Schema.Types.Mixed;

  @Prop()
  biography: mongoose.Schema.Types.Mixed;

  @Prop()
  appearance: mongoose.Schema.Types.Mixed;

  @Prop()
  work: mongoose.Schema.Types.Mixed;

  @Prop()
  connections: mongoose.Schema.Types.Mixed;

  @Prop()
  image: mongoose.Schema.Types.Mixed;
}

export const HeroSchema = SchemaFactory.createForClass(Hero);