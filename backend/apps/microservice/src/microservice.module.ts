import { Module } from '@nestjs/common';
import { MicroserviceController } from './microservice.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HeroModule } from './heroes/hero.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: () => ({
        uri: 'mongodb://localhost/heroes',
      }),
    }),
    HeroModule
  ],
  controllers: [MicroserviceController],
  providers: [],
})

export class MicroserviceModule {}
