import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { HeroService } from './heroes/hero.service';

@Controller()
export class MicroserviceController {
  constructor(private readonly heroService: HeroService) {}

  @MessagePattern('getHello')
  getHero(): Promise<object[]> {
    return this.heroService.getHeros();
  }

  @MessagePattern('saveHero')
  postHero(data: object): object {
    return this.heroService.saveHero(data);
  }
}
