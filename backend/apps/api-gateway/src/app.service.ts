import { Injectable } from '@nestjs/common';
import {
  ClientProxyFactory,
  Transport,
  ClientProxy,
} from '@nestjs/microservices';

@Injectable()
export class AppService {
  private client: ClientProxy;

  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        port: 4000,
      },
    });
  }

  public getHello(): Promise<object> {
    return this.client.send<object>('getHello', '').toPromise();
  }

  public postHero(heroData): Promise<object>{
    return this.client.send<object>('saveHero', heroData).toPromise();
  }
}