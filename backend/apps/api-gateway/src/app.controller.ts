import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getHero(): Promise<object> {
    const helloValue = await this.appService.getHello();
    return helloValue;
  }

  @Post()
  async postHero(@Body() heroData: object): Promise<object> {
    return this.appService.postHero(heroData);
  }
}
