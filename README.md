# Prueba FullStack OmniDoc
## Roberto Espiritu Ramitez

Este repositorio contiene el proyecto de prueba asignado por la empresa OmniDoc la cual se creo la siguiente solucion.

Backend :
- El proyecto de backend esta creado con el framework NestJS. Este contiene una api gateway la cual se conecta a un microservicio. El cual esta encargado de guardar a un heroe en la base de datos y regresar a todos los heroes guardados.

Frontend 
- El frontend esta creado en angular y esta hecho para poder mostrar todos los heroes que manda el EndPoint proporcionada por OmniDoc.
-  El frontend es capas de mostrar y guardar los heroes en la base de datos.

DB
- Se utilizo mongo DB para guardar y consultar informacion de los heroes.

## Caracteristicas


> Nest (NestJS) es un marco de trabajo para construir aplicaciones eficientes y escalables del lado del servidor de Node.js. Utiliza JavaScript progresivo, está construido con TypeScript y es totalmente compatible con él (aunque permite a los desarrolladores codificar en JavaScript puro) y combina elementos de POO (programación orientada a objetos), PF (programación funcional) y FRP (programación reactiva funcional).

> Angular es un marco de diseño de aplicaciones y una plataforma de desarrollo para crear aplicaciones de una sola página eficientes y sofisticadas.

![alt text](https://manticore-labs.com/wp-content/uploads/2019/02/nest.png)
![alt text](https://cdn.freebiesupply.com/logos/thumbs/2x/angular-icon-1-logo.png)



## Instalacion
Se requiere tener instalado ..
[Node.js](https://nodejs.org/)
[NestJS](https://docs.nestjs.com/)
[Angular](https://angular.io/guide/setup-local)
[MongoDB](https://docs.mongodb.com/manual/installation/)


Instalacion de dependencias backend
```sh
cd backend
npm i
```

Instalacion de dependencias frontend

```sh
cd frontend
npm i
```

## Desarrollo

Ya que el proyecto contiene una arquitectura de microservicios, es necesario ejecutar tres comandos los cuales, dos de ellos ejecutan el backend y uno el frontend.

Nota: Se necesitaran entrar a la carpeta backend (cd backend) y ejecutar los sigueintes comandos 
Nota: Es necesario ejecutar el microservicio antes que el api gateway.

Microservicio:

```sh
npm start microservice
```

Api Gateway:

```sh
npm start
```

Ejecucion del frontend 

```sh
ng s
```

## Test

Microservicio
```sh
npm test microservice
```
Api Gateway
```sh
npm test
```
Frontend
```sh
npm test
```


#### Produccion

Para deployar el proyecto en produccion frontend:

```sh
ng build
```

Para deployar el proyecto en produccion api-gateway:

```sh
nest build
```

Para deployar el proyecto en produccion microservicio:

```sh
nest build microservice
```


Para verificar la aplicacion se tiene que acceder a las siguientes rutas desde el navegador de su preferencia.

```sh
Frontend -> GET localhost:4200
Backend -> GET localhost:3000
```

## License

Roberto Espiritu Ramirez
